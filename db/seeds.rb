# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


2.times  do
	Project.create({
    description: Faker::Book.title,
    category: Faker::Book.title,
    post: 1,
    main_image: Faker::Book.title,
    photos: Faker::Book.title
	})
end

1.times do
	User.create({
		name: Faker::Name.name,
		email: Faker::Internet.email,
    password: '123',
    confirm_password:'123',
		date_birth: Faker::Date.in_date_period
	})
end

2.times do 
	Customer.create({
		name: Faker::Name.name,
		email: Faker::Internet.email,
		phone: Faker::PhoneNumber.cell_phone,
    cpf_cnpj: Faker::Code.npi,
    account_mobile: 1,
    cep: Faker::Address.zip_code,
    city: Faker::Address.city,
    state: Faker::Address.city,
		street: Faker::Address.street_name,
		number: Faker::Address.building_number,
		neighborhood: Faker::Address.community,
	})
end

2.times do 
	Employee.create({
		name: Faker::Name.name,
		email: Faker::Internet.email,
    phone: Faker::PhoneNumber.cell_phone,
    cpf: Faker::Code.npi,
		start_date: Faker::Date.in_date_period,
		date_birth: Faker::Date.in_date_period,
    city: Faker::Address.city,
    cep: Faker::Address.zip_code,
    state: Faker::Address.city,
		street: Faker::Address.street_name,
		number: Faker::Address.building_number,
		neighborhood: Faker::Address.community,
	})
end


1.times do
	Demand.create({
    description: Faker::Book.title,
    category: Faker::Book.title,
    attachments: 'não possui',
    customer_id: 1,
    employee_id: 1,
		value: '800',
    status: 'Aguardando pagamento',
		start_date: '2019-12-10',
		end_date: '2020-02-10'

	})
end