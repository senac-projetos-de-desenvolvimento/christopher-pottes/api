class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :cpf_cnpj
      t.boolean :account_mobile
      t.string :street
      t.string :number
      t.string :city
      t.string :state
      t.string :cep
      t.string :number
      t.string :neighborhood
      t.string :complement

      t.timestamps :null => true
    end
  end
end
