class CreateDemands < ActiveRecord::Migration[5.0]
  def change
    create_table :demands do |t|
      t.belongs_to :customer, index: true
      t.belongs_to :employee, index: true
      t.decimal :value
      t.string :attachments
      t.string :description
      t.date :start_date
      t.date :end_date
      t.string :status
      t.string :category

      t.timestamps
    end
  end
end
