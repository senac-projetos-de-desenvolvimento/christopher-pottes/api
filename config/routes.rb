Rails.application.routes.draw do
  
  namespace 'api' do
    namespace 'v1' do
      resources :demands
      resources :projects
      resources :customers
      resources :employees
      resources :providers
      resources :dashboard
      resources :users
      resources :contacts
      resources :calls
			resources :imports
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
