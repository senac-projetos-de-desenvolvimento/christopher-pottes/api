class Demand < ApplicationRecord

  belongs_to :customer, :class_name => 'Customer'
  belongs_to :employee, :class_name => 'Employee'

  validates :description, presence: true
  validates :attachments, presence: true
  validates :value, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :status, presence: true
  validates :category, presence: true
end
