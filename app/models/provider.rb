class Provider < ApplicationRecord
  validates :name, presence: true
  validates :phone, presence: true
  validates :email, presence: true
  validates :cnpj, presence: true
  validates :service, presence: true
  validates :street, presence: true
  validates :cep, presence: true
  validates :number, presence: true
  validates :city, presence: true
  validates :neighborhood, presence: true
  validates :state, presence: true
end
