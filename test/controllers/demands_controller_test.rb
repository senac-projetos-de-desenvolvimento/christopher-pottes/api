require 'test_helper'

class DemandsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @demand = demands(:one)
  end

  test "should get index" do
    get demands_url, as: :json
    assert_response :success
  end

  test "should create demand" do
    assert_difference('Demand.count') do
      post demands_url, params: { demand: { attachments: @demand.attachments, category: @demand.category, customer_id: @demand.customer_id, description: @demand.description, employee_id: @demand.employee_id, end_date: @demand.end_date, start_date: @demand.start_date, status: @demand.status, value: @demand.value } }, as: :json
    end

    assert_response 201
  end

  test "should show demand" do
    get demand_url(@demand), as: :json
    assert_response :success
  end

  test "should update demand" do
    patch demand_url(@demand), params: { demand: { attachments: @demand.attachments, category: @demand.category, customer_id: @demand.customer_id, description: @demand.description, employee_id: @demand.employee_id, end_date: @demand.end_date, start_date: @demand.start_date, status: @demand.status, value: @demand.value } }, as: :json
    assert_response 200
  end

  test "should destroy demand" do
    assert_difference('Demand.count', -1) do
      delete demand_url(@demand), as: :json
    end

    assert_response 204
  end
end
